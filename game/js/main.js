const movesPerTurn = 4;
const lands = {
    west: { // Americas
        Aurumia: {
            name: "Aurumia", // US-ish area
            population: 25000,
            birthMultiplier: 1.20,
            deathMultiplier: 0.85,
            power: 0.6,
            doctors: 0,
            teachers: 0,
            resources: {
                coal: 2,
                crops: 7,
                gold: 14,
                silk: 0,
                silver: 5,
                spices: 5
            },
            costs: {
                coal: 1,
                crops: 6,
                gold: 1,
                silk: 0,
                silver: 1,
                spices: 0
            }
        },
        Tropicia: {
            name: "Tropicia", // West Indies
            population: 2100,
            birthMultiplier: 1.2,
            deathMultiplier: 0.88,
            power: 0.6,
            doctors: 0,
            teachers: 0,
            resources: {
                coal: 0,
                crops: 5,
                gold: 2,
                silk: 0,
                silver: 1,
                spices: 7
            },
            costs: {
                coal: 0,
                crops: 3,
                gold: 1,
                silk: 0,
                silver: 0,
                spices: 2
            }
        }
    },
    east: { // Asia
        Sericumia: {
            name: "Sericumia", // Far east, China?
            population: 370000,
            birthMultiplier: 1.34,
            deathMultiplier: 0.65,
            power: 0.3,
            doctors: 0,
            teachers: 0,
            resources: {
                coal: 3,
                crops: 11,
                gold: 1,
                silk: 10,
                silver: 2,
                spices: 7
            },
            costs: {
                coal: 2,
                crops: 8,
                gold: 1,
                silk: 6,
                silver: 1,
                spices: 6
            }
        },
        Centralisia: {
            name: "Centralisia", // Middle east or East India
            population: 13600,
            birthMultiplier: 1.32,
            deathMultiplier: 0.84,
            power: 0.9,
            doctors: 0,
            teachers: 0,
            resources: {
                coal: 0,
                crops: 6,
                gold: 1,
                silk: 0,
                silver: 1,
                spices: 8
            },
            costs: {
                coal: 0,
                crops: 2,
                gold: 0,
                silk: 0,
                silver: 0,
                spices: 4
            }
        }
    }
};
const historyText = {
    colonise: "Colonised ${name}",
    recruit: "Recruited ${amount} from ${name}",
    doctor: "Trained doctor${name ? ' and sent to ' + name : ''}",
    teacher: "Trained teacher${name ? ' and sent to ' + name : ''}",
}

Vue.component("action", {
    props: ["name", "alt"],
    template: `
    <button class="action" :title="alt">
        {{ name }}
    </button>
    `
});

Vue.component("resources", {
    props: ["kingdom"],
    methods: {
        getGeneratedResource: function (name) {
            let x = this.kingdom.resources[name];
            for (let i in this.kingdom.colonies) {
                x += this.kingdom.colonies[i].resources[name];
            }
            return x;
        },
        getResourceCost: function (name) {
            let x = this.kingdom.costs[name];
            for (let i in this.kingdom.colonies) {
                x += this.kingdom.colonies[i].costs[name];
            }
            return x;
        },
    },
    template: `
        <div class="infobox">
            <h3>Resources</h3>
            <div class="flex-container">
                <table>
                    <thead>
                        <tr>
                            <td>Resource</td>
                            <td>Stored</td>
                            <td>Generated</td>
                            <td>Cost</td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr v-for="(stored, name) in kingdom.storage">
                            <td class="caps">{{ name }}</td>
                            <td>{{ stored }}</td>
                            <td>{{ getGeneratedResource(name) }}</td>
                            <td>{{ getResourceCost(name) }}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    `
});

Vue.component("population", {
    props: ["totalPopulation", "kingdom"],
    template: `
    <div class="infobox">
        <h3>Population</h3>
        <span>Total population: {{ totalPopulation }}</span>
        <li>{{ kingdom.name }} - {{ kingdom.localPopulation }}
        <li v-for="colony in kingdom.colonies">
            {{ colony.name }} - {{ colony.population }}
        </li>
    </div>
    `,
});

Vue.component("landdetails", {
    props: ["land", "kingdom", "queue"],
    template: `
    <li class="land-details">
        <span class="clickable" @click="isExpanded = !isExpanded">{{ land.name }}</span>
        <transition name="grow-section">
            <div v-if="isExpanded">
                <span>Population: {{ land.population }}</span>
                <span>Birth rate: <strong>{{ birthRate }} per year</strong></span>
                <span>Death rate: <strong>{{ deathRate }} per year</strong></span>
                <span>Doctors: <strong>{{ land.doctors }}</strong></span>
                <span>Teachers: <strong>{{ land.teachers }}</strong></span>
                <hr />
                <span class="caps" v-for="(amount, resource) in land.resources">{{ resource }}: {{ amount }}</span>
                <hr />
                <action v-if="isColonised && localQueue.indexOf('recruit') == -1" @click.native="queueRecruit" name="Recruit workers" :alt="recruitMessage"></action>
                <action v-if="isColonised && localQueue.indexOf('doctor') == -1" @click.native="queueDoctor" name="Send a doctor" :alt="doctorMessage"></action>
                <action v-if="isColonised && localQueue.indexOf('teacher') == -1" @click.native="queueTeacher" name="Send a teacher" :alt="teacherMessage"></action>
                <span v-if="!isColonised">Estimated men needed to colonise: {{ estForce }}</span>
                <span v-if="canColonise && !safeToColonise"><strong>You might not have enough men to safely colonise this land!</strong></span>
                <action v-if="canColonise" @click.native="queueThis" name="Colonise" :alt="coloniseMessage"></action>
            </div>
        </transition>
    </li>
    `,
    data: function () {
        return {
            isExpanded: false,
            localQueue: []
        }
    },
    computed: {
        estForce: function () {
            return Math.ceil(this.land.population * this.land.power);
        },
        birthRate: function () {
            return Math.round((this.land.birthMultiplier - 1) * this.land.population);
        },
        deathRate: function () {
            return Math.round((1 - this.land.deathMultiplier) * this.land.population);
        },
        isColonised: function () {
            return this.land.name in this.kingdom.colonies;
        },
        isQueued: function () {
            return this.queue.indexOf(this.land.name) > -1;
        },
        canColonise: function () {
            return !(this.isColonised || this.isQueued);
        },
        safeToColonise: function () {
            return (this.estForce * 1.02 < this.kingdom.localPopulation * 0.95);
        },
        coloniseMessage: function () {
            return `Send troops to ${this.land.name} to settle there. Some of your population will be lost at war or will remain there.`;
        },
        recruitMessage: function () {
            return `Recruit some willing workers from ${this.land.name} and bring them over to ${this.kingdom.name}! This costs 2 gold.`;
        },
        doctorMessage: function () {
            return `Train up a doctor from ${this.kingdom.name} and send them over to ${this.land.name}! This costs 5 silver but improves the birth and survival rates here.`;
        },
        teacherMessage: function () {
            return `Train up a teacher from ${this.kingdom.name} and send them over to ${this.land.name}! This costs 30 silver but improves the yearly resource yield here.`
        }
    },
    methods: {
        queueThis: function () {
            this.localQueue.push("colonise");
            this.$emit("queue");
        },
        queueRecruit: function () {
            this.localQueue.push("recruit");
            this.$emit("queueRecruit");
        },
        queueDoctor: function () {
            this.localQueue.push("doctor");
            this.$emit("queueDoctor");
        },
        queueTeacher: function () {
            this.localQueue.push("teacher");
            this.$emit("queueTeacher");
        }
    }
});

Vue.component("colonies", {
    props: ["kingdom"],
    template: `
    <div class="infobox">
        <h3>Colonies</h3>
        <landdetails v-for="(colony, name) in kingdom.colonies"
            :key="name" :land="colony" :kingdom="kingdom"
            @queueRecruit="queueRecruit(name)" @queueDoctor="queueDoctor(name)"
            @queueTeacher="queueTeacher(name)"></landdetails>
    </div>
    `,
    methods: {
        queueRecruit: function (land) {
            this.$emit("queue", JSON.stringify({
                type: "recruit",
                data: {
                    land: land,
                    amount: Math.floor(Math.random() * 0.8 * this.kingdom.colonies[land].population)
                }
            }));
        },
        queueDoctor: function (land) {
            this.$emit("queue", JSON.stringify({
                type: "doctor",
                data: {
                    land: land
                }
            }));
        },
        queueTeacher: function (land) {
            this.$emit("queue", JSON.stringify({
                type: "teacher",
                data: {
                    land: land
                }
            }));
        }
    }
});

Vue.component("foreignlands", {
    props: ["kingdom"],
    template: `
    <div class="infobox">
        <h3>Foreign Lands</h3>
        <div v-for="(places, area) in lands">
            <h4>{{ area }}</h4>
            <landdetails v-for="(place, name) in places" :key="name" :land="place" :kingdom="kingdom" :queue="queue.colonise" @queue="queueColonise(area, name)"></landdetails>
        </div>
    </div>
    `,
    data: function () {
        return {
            lands: Object.assign({}, lands),
            queue: {
                colonise: []
            },
            visibleLand: ""
        }
    },
    methods: {
        queueColonise: function (area, name) {
            this.queue.colonise.push(name);
            this.$emit("queue", JSON.stringify({
                type: "colonise",
                data: Object.assign({}, this.lands[area][name])
            }));
        }
    }
});

Vue.component("kingdom", {
    props: ["kingdom", "monarch"],
    template: `
    <div class="infobox homeland">
        <h3>Homeland</h3>
        <h4>{{ kingdom.name }}</h4>
        <div class="flex-popup">
            <span>Birth rate: <strong>{{ birthRate }} per year</strong></span>
            <span>Death rate: <strong>{{ deathRate }} per year</strong></span>
            <span>Doctors: <strong>{{ kingdom.doctors }}</strong></span>
            <span>Teachers: <strong>{{ kingdom.teachers }}</strong></span>
            <action v-if="queue.indexOf('doctor') == -1" name="Train a doctor" :alt="doctorMessage" @click.native="queueDoctor"></action>
            <action v-if="queue.indexOf('teacher') == -1" name="Train a teacher" :alt="teacherMessage" @click.native="queueTeacher"></action>
        </div>
        <h4>{{ monarch.name }}</h4>
        <div class="flex-popup">
            <span>Age: {{ monarch.age }}</span>
        </div>
    </div>
    `,
    computed: {
        birthRate: function () {
            return Math.round((this.kingdom.birthMultiplier - 1) * this.kingdom.localPopulation);
        },
        deathRate: function () {
            return Math.round((1 - this.kingdom.deathMultiplier) * this.kingdom.localPopulation);
        },
        doctorMessage: function () {
            return `Train up a doctor to care for ${this.kingdom.name}'s population! This costs 5 silver but improves the birth and survival rates here.`;
        },
        teacherMessage: function () {
            return `Train up a teacher to teach ${this.kingdom.name}'s population! This costs 30 silver but improves the yearly resource yield here.`;
        }
    },
    methods: {
        queueDoctor: function (land) {
            this.$emit("queue", JSON.stringify({
                type: "doctor",
                data: {}
            }));
            this.queue.push("doctor");
        },
        queueTeacher: function (land) {
            this.$emit("queue", JSON.stringify({
                type: "teacher",
                data: {}
            }));
            this.queue.push("teacher");
        }
    },
    data: function () {
        return {
            queue: []
        }
    }
});

Vue.component("ingame", {
    props: ["monarchName", "kingdomName"],
    template: `
    <div>
        <div id="statusbar" v-if="!loseMessage">
            <span class="clickable" @click="currentBox = 'kingdom'"><strong>{{ monarch.name }}</strong> of <strong>{{ kingdom.name }}</strong></span>
            <span>Year <strong>{{ turn }}</strong></span>
            <span class="clickable" @click="currentBox = 'population'">Population: <strong>{{ totalPopulation }}</strong></span>
            <span class="clickable" v-if="Object.keys(kingdom.colonies).length > 0" @click="currentBox = 'colonies'">Colonies: <strong>{{ Object.keys(kingdom.colonies).length }}</strong></span>
            <span class="clickable" @click="currentBox = 'resources'">Resources...</span>
            <span class="clickable" @click="currentBox = 'foreignlands'">Explore...</span>
            <span class="clickable" v-if="queue.length > 0" @click="advanceTurn">Next</span>
        </div>
        <div id="container" v-if="!loseMessage">
            <transition name="infobox" mode="out-in">
                <component :is="currentBox" :kingdom="kingdom" :monarch="monarch" :total-population="totalPopulation" @queue="queueAction"></component>
            </transition>
        </div>
        <transition name="menu">
            <div class="menu lose-message" v-if="loseMessage">
                <h1>Game over!</h1>
                <h2>{{ loseMessage }}</h2>
                <span>Your kingdom survived for {{ turn }} years.</span>
                <span v-if="kingdom.colonies.length > 0">Your empire consisted of:</span>
                <li v-for="(colony, name) in kingdom.colonies">{{ name }}</li>
                <action v-if="showHistory" name="Show/hide history" alt="Click to show/hide history." @click.native="showHistory = !showHistory"></action>
                <div v-if="showHistory">
                    <li v-for="year in history">
                        {{ log(history, year) }}
                        <li v-for="action in year">{{ getHistoryText(action) }}</li>
                    </li>
                </div>
                <small>Reload to try again.</small>
            </div>
        </transition>
    </div>
    `,
    methods: {
        log: function (...data) {
            console.log.apply(this, data);
        },
        getHistoryText: function (action) {
            let unparsed = historyText[action.type];
            let parsed;
            with (action.data) {
                parsed = eval(unparsed);
            }
            return parsed;
        },
        deductResource: function (type, amount) {
            if (this.kingdom.storage[type] < amount) return false;
            this.kingdom.storage[type] -= amount;
            this.stats.spent[type] += amount;
            return true;
        },
        coloniseLand: function (data) {
            if (data.name in this.kingdom.colonies) return;
            let foreignForce = Math.round(data.population * data.power);
            this.kingdom.localPopulation -= foreignForce * (Math.random() + 0.2);
            this.kingdom.colonies[data.name] = data;
        },
        recruitWorkers: function (data) {
            if (!this.deductResource("gold", 2)) return;
            this.kingdom.localPopulation += data.amount;
            this.kingdom.colonies[data.land].population -= data.amount;
            this.stats.recruited += data.amount;
        },
        sendDoctor: function (data) {
            if (!this.deductResource("silver", 5)) return;
            if (data.name) {
                this.kingdom.localPopulation -= 1;
                this.kingdom.colonies[data.name].doctors += 1
            } else {
                this.kingdom.doctors += 1;
            }
            this.stats.doctors += 1;
        },
        sendTeacher: function (data) {
            if (!this.deductResource("silver", 30)) return;
            if (data.name) {
                this.kingdom.localPopulation -= 1;
                this.kingdom.colonies[data.name].teachers += 1;
            } else {
                this.kingdom.teachers += 1;
            }
            this.stats.teachers += 1;
        },
        queueAction: function (actionJson) {
            this.queue.push(JSON.parse(actionJson));
        },
        calcResources: function () {
            for (let resource in this.kingdom.resources) {
                this.kingdom.storage[resource] += this.kingdom.resources[resource] * (1.05 ^ (this.kingdom.teachers + 1));
                this.kingdom.storage[resource] -= this.kingdom.costs[resource];
            }
            for (let n in this.kingdom.colonies) {
                let colony = this.kingdom.colonies[n];
                for (let resource in colony.resources) {
                    this.kingdom.storage[resource] += colony.resources[resource] + (colony.power ^ (colony.teachers + 1));
                    this.kingdom.storage[resource] -= colony.costs[resource];
                }
            }
            for (let resource in this.kingdom.storage) {
                this.kingdom.storage[resource] = Math.floor(this.kingdom.storage[resource] * 100) / 100;
                if (this.kingdom.storage[resource] < 0) this.loseGame(`You ran out of ${resource}!`);
            }
        },
        calcPopulation: function () {
            this.kingdom.localPopulation = Math.floor((this.kingdom.localPopulation
                * this.kingdom.birthMultiplier * this.kingdom.deathMultiplier) + (1.02 ^ (this.kingdom.doctors + 1)));
            for (let i in this.kingdom.colonies) {
                let colony = this.kingdom.colonies[i];
                colony.population = Math.floor((colony.population * colony.birthMultiplier * colony.deathMultiplier)
                    + (1.02 ^ (colony.doctors + 1)));
            }
            if (this.kingdom.localPopulation < 1) this.loseGame("All of your local population died or moved away.");
        },
        commitHistory: function () {
            let year = [];
            let totalActions = Math.min(movesPerTurn, this.queue.length)
            this.history.push(year);
            for (let i = 0; i < totalActions; i++) {
                let action = this.queue.shift();
                year.push(action);
                switch (action.type) {
                    case "colonise":
                        this.coloniseLand(action.data);
                        break;
                    case "recruit":
                        this.recruitWorkers(action.data);
                        break;
                    case "doctor":
                        this.sendDoctor(action.data);
                        break;
                    case "teacher":
                        this.sendTeacher(action.data);
                        break;
                    default:
                        this.log(`Unsupported action "${action.type}"`, action.data);
                        break;
                }
            }
        },
        advanceTurn: function () {
            this.commitHistory();
            this.calcResources();
            this.calcPopulation();
            this.turn += 1;
            this.monarch.age += 1;
            this.currentBox = "";
        },
        loseGame: function (message) {
            this.loseMessage = message;
        }
    },
    computed: {
        totalPopulation: function () {
            let pop = this.kingdom.localPopulation;
            for (let i in this.kingdom.colonies) {
                pop += this.kingdom.colonies[i].population;
            }
            return pop;
        }
    },
    data: function () {
        return {
            turn: 1,
            history: [],
            queue: [],
            kingdom: {
                name: this.kingdomName,
                colonies: {},
                localPopulation: 10000,
                birthMultiplier: 1.19,
                deathMultiplier: 0.9,
                resources: {
                    coal: 4,
                    crops: 7,
                    gold: 0,
                    silk: 0,
                    silver: 2,
                    spices: 0
                },
                storage: {
                    coal: 3,
                    crops: 38,
                    gold: 2,
                    silk: 0,
                    silver: 6,
                    spices: 0
                },
                costs: {
                    coal: 2,
                    crops: 10,
                    gold: 0,
                    silk: 0,
                    silver: 0,
                    spices: 0,
                },
                doctors: 1,
                teachers: 1
            },
            monarch: {
                name: this.monarchName,
                age: 20
            },
            currentBox: "",
            loseMessage: "",
            stats: {
                doctors: 0,
                recruited: 0,
                spent: {
                    coal: 0,
                    crops: 0,
                    gold: 0,
                    silk: 0,
                    silver: 0,
                    spices: 0
                }
            },
            showHistory: false
        }
    }
});

const game = new Vue({
    el: "#game",
    data: {
        started: false,
        monarchName: "King Jeff the Great",
        kingdomName: "Kingdomland",
    },
    methods: {
        start: function () {
            this.started = true;
        }
    }
});
